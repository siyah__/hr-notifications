package za.co.omnipresent.Notifications.Config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import za.co.omnipresent.Notifications.Service.notificationService;
import za.co.omnipresent.Notifications.Service.notificationServiceImpl;

@Configuration
public class BeanConfig {

    @Bean
    public notificationService notification(){
        return new notificationServiceImpl();
    }

}
