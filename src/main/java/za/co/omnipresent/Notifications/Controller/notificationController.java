package za.co.omnipresent.Notifications.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import za.co.omnipresent.Notifications.Service.notificationService;

import java.io.IOException;


@RestController
@RequestMapping(value = "/email")
public class notificationController {

    private notificationService notification;

    @RequestMapping(method = RequestMethod.POST, value = "/send/{recipient}/with/{subject_}/and/{contents}")
    public @ResponseBody int sendEmail(@PathVariable String recipient, @PathVariable String subject_, @PathVariable String contents) throws IOException
    {
       return notification.sendSimpleMail(recipient, subject_, contents);
    }

    @Autowired
    public void setNotificationService(notificationService notification){
        this.notification = notification;

    }

}
