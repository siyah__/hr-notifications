package za.co.omnipresent.Notifications.Service;

import java.io.IOException;

public interface notificationService {

    int sendSimpleMail(String recipient, String subject_, String contents) throws IOException;
}
