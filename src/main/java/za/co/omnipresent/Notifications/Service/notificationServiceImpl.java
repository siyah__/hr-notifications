package za.co.omnipresent.Notifications.Service;

import com.sendgrid.*;
import org.springframework.beans.factory.annotation.Value;
import za.co.omnipresent.Notifications.Exceptions.BadRequest;

import java.io.IOException;

public class notificationServiceImpl implements notificationService {

    @Value("${sendgrid.api.key}")
    private String apiKey;
    @Value("${sendgrid.mail.from}")
    private String sentFrom;

    public int sendSimpleMail(String recipient, String subject_, String contents) throws IOException
    {
        Email from = new Email(sentFrom);
        String subject = subject_;
        Email to = new Email(recipient);
        Content content = new Content("text/plain", contents);
        Mail mail = new Mail(from, subject, to, content);

        SendGrid sg = new SendGrid(apiKey);
        Request request = new Request();

        Response response;
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            response = sg.api(request);
            return response.getStatusCode();
        } catch (IOException ex) {
            if(ex.getMessage().contains("Does not contain a valid address.")) {
                throw new BadRequest("Does not contain a valid address.");
            }
            if(ex.getMessage().contains("returned status Code 400") ) {
                throw new BadRequest("returned status Code 400");
            }
            throw new InternalError("returned status Code 400");
        }
    }
}
